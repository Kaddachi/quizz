# Generated by Django 2.0.7 on 2018-08-27 14:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('exam', '0005_auto_20180827_1411'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='explanation',
            field=models.CharField(blank=True, default='', max_length=1000, null=True),
        ),
    ]
